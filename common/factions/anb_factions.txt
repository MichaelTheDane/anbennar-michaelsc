########################################
# Adventurer Factions
########################################

#Fighting
adv_marchers =
{
	monarch_power = MIL
	always = yes

	modifier = 
	{
		diplomatic_upkeep = -1
		idea_cost = 0.1
		
		diplomatic_reputation = 1
		land_morale = 0.05
		prestige_from_land = 0.5
		land_forcelimit_modifier = 0.2
	}
}

#Colonising and developing provinces
adv_pioneers =
{
	monarch_power = DIP
	always = yes

	modifier = 
	{
		administrative_efficiency = -0.2
		global_manpower_modifier = -0.2
	
		development_cost = -0.1
		land_attrition = -0.1
		global_colonial_growth = 20
		colonist_placement_chance = 0.1
	}
}

#Trading
adv_fortune_seekers =
{
	monarch_power = ADM
	always = yes

	modifier = 
	{
		discipline = -0.05
		diplomatic_reputation = -1
		
		caravan_power = 0.2
		global_trade_goods_size_modifier = 0.10
		diplomatic_upkeep = 1
		build_cost = -0.10
	}
}