government = theocratic_government
government_rank = 1
primary_culture = zanite
add_accepted_culture = sun_elf
religion = bulwari_sun_cult
technology_group = tech_bulwari
historical_friend = F25 #historical overlord
capital = 631

1405.9.1 = {
	monarch = {
		name = "Zaid of Azkabar"
		birth_date = 1404.12.3
		adm = 2
		dip = 3
		mil = 0
	}
}
