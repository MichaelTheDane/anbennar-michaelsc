government = feudal_monarchy
government_rank = 1
primary_culture = esmari
religion = regent_court
technology_group = tech_cannorian
capital = 267 # Themarenn
national_focus = DIP

1422.1.1 = { set_country_flag = lilac_wars_moon_party }

1422.1.2 = { set_country_flag = is_a_county }

1440.5.3 = {
	monarch = {
		name = "Vernell III"
		dynasty = "s�l Themarenn"
		birth_date = 1429.2.6
		adm = 4
		dip = 2
		mil = 2
	}
}