government = feudal_monarchy
government_rank = 1
primary_culture = esmari
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 268

1422.1.1 = { set_country_flag = lilac_wars_moon_party }

1422.2.4 = {
	monarch = {
		name = "Aldres III"
		dynasty = "s�l Bennon"
		birth_date = 1411.4.11
		adm = 2
		dip = 4
		mil = 6
	}
}