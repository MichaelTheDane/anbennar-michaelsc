government = feudal_monarchy
government_rank = 1
primary_culture = esmari
religion = regent_court
technology_group = tech_cannorian
capital = 280 # Estallen
national_focus = DIP

1422.1.1 = { set_country_flag = lilac_wars_rose_party }

1438.1.5 = {
	monarch = {
		name = "Lucian IV"
		dynasty = "s�l Estallen"
		birth_date = 1413.7.3
		adm = 3
		dip = 3
		mil = 2
	}
}

1438.5.16 = {
	heir = {
		name = "Cecill"
		monarch_name = "Cecill III"
		dynasty = "s�l Estallen"
		birth_date = 1438.5.16
		death_date = 1480.1.1
		claim = 95
		adm = 6
		dip = 3
		mil = 1
	}
}