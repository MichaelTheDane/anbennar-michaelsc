#26 - Halland | 

owner = A22
controller = A22
add_core = A22
culture = west_damerian
religion = regent_court

hre = yes

base_tax = 6
base_production = 6
base_manpower = 3

trade_goods = cloth
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = center_of_trade_modifier
	duration = -1
}