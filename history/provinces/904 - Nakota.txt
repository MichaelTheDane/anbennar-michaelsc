#904 - Nakota

owner = A72
controller = A72
add_core = A72
culture = arannese
religion = regent_court

hre = yes

base_tax = 7
base_production = 7
base_manpower = 5

trade_goods = cloth

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = inland_center_of_trade_modifier
	duration = -1
}