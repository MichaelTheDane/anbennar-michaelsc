#52 - Altmark

owner = A31
controller = A31
add_core = A31
culture = anbenncoster
religion = regent_court

hre = yes

base_tax = 13
base_production = 13
base_manpower = 9

trade_goods = naval_supplies

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = center_of_trade_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = luna_estuary_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = human_minority_coexisting_large
	duration = -1
}