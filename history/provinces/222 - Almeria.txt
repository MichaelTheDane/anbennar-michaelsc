#222 - Almeria |

owner = A13
controller = A13
add_core = A13
culture = gawedi
religion = regent_court

hre = no

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = iron

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
discovered_by = tech_orcish
