#308 - Yaroslavl

owner = A73
controller = A73
add_core = A73
culture = silver_dwarf
religion = regent_court

hre = yes

base_tax = 8
base_production = 8
base_manpower = 6

trade_goods = iron
capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
