#81 - Hessen | 

owner = A16
controller = A16
add_core = A16
culture = high_lorentish
religion = regent_court

hre = no

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = wine

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

