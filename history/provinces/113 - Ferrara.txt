#113 - Ferrara

owner = A02
controller = A02
culture = derannic
religion = regent_court
hre = no
base_tax = 6 
base_production = 6 
trade_goods = cloth
base_manpower = 4
capital = "" 
is_city = yes
fort_15th = yes
add_core = A02

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
