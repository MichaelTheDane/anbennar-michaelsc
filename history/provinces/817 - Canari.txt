# No previous file for Canari
owner = B04
controller = B04
add_core = B04
culture = high_lorentish
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish