# A province can only belong to one region!

# random_new_world_region is used for RNW. Must be first in this list.
random_new_world_region = {
}

########################################
# AELANTIR		                       #
########################################

north_aelantir_region = {
	areas = {
		areaae1_area
		areaae2_area
	}
}

endralliande_region = {
	areas = {
		areaae3_area
		areaae4_area
		areaae5_area
		areaae6_area
		areaae7_area
		areaae8_area
		areaae9_area
		areaae10_area
		areaae11_area
		munasport_area
		inner_endralliande_area
	}
}

ravenous_isle_region = {
	areas = {
		areaae12a_area
		areaae12b_area
		areaae13_area
		areaae14_area
		areaae15_area
	}
}

ruined_isles_region = {
	areas = {
		arears1_area
		arears2_area
		arears3_area
		arears4_area
		arears5_area
		arears6_area
		arears7_area
		arears8_area
		arears9_area	#these four should be changed when we do the south bit of that place
		arears10_area
		arears11_area
		arears12_area
		wanderers_gate_area
		calamity_islands_area
		areaae16_area
	}
}

trollsbay_region = {
	areas = {
		areatb1_area
		areatb2_area
		areatb3_area
		areatb4_area
		areatb5_area
		areatb6_area
		areatb7_area
		areatb8_area
		areatb9_area
		areatb10_area
		areatb15_area
	}
}

reapers_coast_region = {
	areas = {
		areatb30_area
		areatb32_area
		areatb33_area
	}
}

dalaire_region = {
	areas = {
		stalwart_outpost_area
	}
}

bloodgroves_region = {
	areas = {
		south_dalairey_coast_area
		areatb13_area
		areatb14_area
		bloodbrook_area
		areatb21_area
		areatb22_area
	}
}

########################################
# GERUDIA		                       #
########################################

east_gerudia_region = {
	areas = {
		areag1_area
		areag2_area
		areag3_area
		areag4_area
		areag5_area
		areag6_area
		areag7_area
		giants_manse_area
	}
}

west_gerudia_region = {
	areas = {
		areag8_area
		areag9_area
		areag10_area
		areag11_area
		areag12_area
		areag13_area
		areag14_area
		areag15_area
		areag16_area
		areag17_area
		areag18_area
		lonely_isle_area
	}
}

########################################
# WESTERN CANNOR                       #
########################################

isles_of_lament_region = {
	areas = {
		far_isle_area
	}
}

lencenor_region = {
	areas = {
		venail_area
		upper_bloodwine_area
		lower_bloodwine_area
		sorncost_vines_area
		sornhills_area
		shrouded_coast_area
		ionnidar_area
		redglades_area
		ruby_mountains_area
		deranne_area
		northern_flats_area
		southern_flats_area
		darom_area
		eastern_winebay_area
		upper_winebay_area
		lower_winebay_area
		southroy_area
	}
}

small_country_region = {
	areas = {
		viswall_area
		roysfort_area
		the_borders_area
		thomsbridge_area
		applefields_area
		lorenans_reach_area
		lorentish_approach_area
		pearpoint_area
		norley_area
	}
}

dragon_coast_region = {
	areas = {
		gnomish_pass_area
		nimscodd_area
		dragonpoint_area
		dragondowns_area
		dragonspine_area
		dragonheights_area
		storm_isles_area
		iochand_area
		reaver_coast_area
		dragonhills_area
	}
}

west_dameshead_region = {
	areas = {
		wesdam_area
		neckcliffe_area
		damespearl_area
		pearlywine_area
		carneteria_area
		windtower_area
		tretunica_area
		exwes_area
		woodwell_area
	}
}

east_dameshead_region = {
	areas = {
		west_damesear_area
		east_damesear_area
		eastneck_area
		damerian_dales_area
		silverwoods_area
		menibor_loop_area
		middle_luna_area
		the_commons_area
		upper_luna_area
		verne_area
		wyvernmark_area
		galeinn_area
		heartlands_area
	}
}

the_borders_region = {
	areas = {
		wexhills_area
		east_bisan_area
		west_bisan_area
		ottocam_area
		greater_bardswood_area
		eastborders_area
		gnollsgate_area
		gisden_area
		arannen_area
		rhinmond_area
		hawkfields_area
		antir_drop_area
		highcliff_area
	}
}

forlorn_vale_region = {
	areas = {
		north_ibevar_area
		south_ibevar_area
		silent_repose_area
		teagansfield_area
		area20_area
		rotwall_area
		area31_area
	}
}

esmaria_region = {
	areas = {
		konwell_area
		ryalanar_area
		high_esmar_area
		low_esmar_area
		hearthswood_area
		bennonhill_area
		cann_esmar_area
		songbarges_area
		ashfields_area
	}
}

businor_region = {
	areas = {
		lorbet_area
		mountainway_area
		busilar_area
		busilari_straits_area
		hapiande_area
		khenak_area
		isle_of_tef_area
	}
}

alenic_frontier_region = {
	areas = {
		westmoor_proper_area
		moorhills_area
		beronmoor_area
		westmounts_area
		alenic_expanse_area
		gawed_area
		south_alen_area
		alenvord_area
		balvord_area
		jonsway_area
		arbaran_area
		golden_plains_area
		cestir_area
		storm_coast_area
	}
}

alenic_reach_region = {
	areas = {
		mawriver_area
		serpentshead_area
		northern_greatwoods_area
		areafa1_area
		drowned_giant_isles_area
		west_chillsbay_area
		east_chillsbay_area
		areafa2_area
		areafa3_area
		areafa4_area
		areafa5_area
		areafa6_area
		vrorenmarch_area
	}
}

deepwoods_region = {
	areas = {
		lonely_meadow_area
	}
}

damescrown_region = {
	areas = {
		damescrown_area
		floodmarches_area
		vertesk_area
		dames_forehead_area
		beepeck_area
		derwing_area
		heartland_borders_area
		ardail_island_area
	}
}

dostanor_region = {
	areas = {
		corvurian_plains_area
		baldostan_area
		central_corvuria_area
		blackwoods_area
	}
}

daravans_folly_region = {
	areas = {
		dreadmire_area
		flooded_coast_area
	}
}

inner_castanor_region = {
	areas = {
		castonath_area
		upper_nath_area
		lower_nath_area
		westgate_area
		southgate_area
		trialmount_area
		steelhyl_area
		area68_area
		area71_area
		area72_area
		area73_area
		area74_area
		area75_area
		area76_area
		area77_area
		area78_area
		area80_area
		area84_area
	}
}

south_castanor_region = {
	areas = {
		burnoll_area
		dostans_way_area
		marrhold_area
		area53_area
		area54_area
		area55_area
		area56_area
		area57_area
		area58_area
		area59_area
		area60_area
		area61_area
		area62_area
		area63_area
		area64_area
		area65_area
		area66_area
		area81_area
		area82_area
		area83_area
	}
}

west_castanor_region = {
	areas = {
		balmire_area
		westwall_approach_area
		middle_alen_area
		athfork_area
		area42_area
		area43_area
		area44_area
		area45_area
		area46_area
		area47_area
		area48_area
		area49_area
		area50a_area
		area50b_area
		area51_area
		area52_area
	}
}


akan_region = {
	areas = {
		deshak_area
		ekha_area
		middle_akan_area
		east_akan_area
		central_akan_area
		west_akan_area
	}
}

north_salahad_region = {
	areas = {
		area32_area
		area33_area
		area34_area
		coast_of_tears_area
		mothers_delta_area
		area35_area
		lower_sorrow_area
		kheterat_proper_area
		upper_sorrow_area
		area37_area
		area38_area
		elizna_area
		area39_area
	}
}

gol_region = {
	areas = {
		golkora_stretch_area
		gnollakaz_area
		area36_area
	}
}

########################################
# Bulwar				  		#
########################################

overmarch_region = {
	areas = {
		west_overmarch_area
		east_overmarch_area
		south_overmarch_area
	}
}

bahar_region = {
	areas = {
		crathanor_area
		reuyel_area
		medbahar_area
		area_f1_area
		tungr_mountains_area
		aqatbahar_area
		azka_sur_area
		bahar_szel_uak_area
		area_f3_area
	}
}

bulwar_proper_region = {
	areas = {
		inner_drolas_area
		outer_drolas_area
		areab1_area
		brasan_area
		areab2_area
		areab3_area
		areab4_area
		areab5_area
		areab6_area
		areab7_area
		areab8_area
		areab9_area
		areab10_area
		areab11_area
		areab12_area
		areab13_area
		bulwar_area
		areab15_area
		areab16_area
		areab17_area
		areab19_area
		areab20_area
		areab21_area
		areab22_area	
	}
}

harpy_hills_region = {
	areas = {
		area_hh0a_area
		area_hh0b_area
		areahh8_area
		alyzksaan_area
		areahh1_area
		areahh2_area
		areahh3_area
		areahh4_area
		areahh5_area
		areahh6_area
		areahh7_area
		jorkad_lake_area
	}
}

far_bulwar_region = {
	areas = {
		areab23_area	
		areab24_area
		areab25_area	
		areab26_area	
		areab27_area	
		areab28_area	
		areab29_area	
		areab30_area	
		areab31_area	
		areab18_area
	}
}

#Sea Regions
north_aelantir_coast_region = {
	areas = {
		north_aelantir_coast_area
	}
}

south_aelantir_coast_region = {
	areas = {
		south_aelantir_coast_area
		leechdens_area
	}
}

north_uelos_lament_ocean_region = {
	areas = {
		west_salahad_sea_area
		edillions_relief_area
		far_sea_area
		the_great_expanse_area
		banished_sea_area
		coast_of_endrallian_area
	}
}

south_uelos_lament_ocean_region = {
	areas = {
		south_uelos_lament_sea_area
		far_uelos_lament_sea_area
	}
}

south_sarhal_coast_region = {
	areas = {
		splintered_archipelago_area
		kedwali_coast_area
	}
}

west_salahad_coast_region = {
	areas = {
		west_salahad_sea_area
	}
}

cleaved_sea_coast_region = {
	areas = {
		sea3_area
		cleaved_sea_area
	}
}

north_insyaa_ocean_region = {
	areas = {
		north_insyaa_sea_area
		bay_of_insyaa_area
		barrier_islands_area
	}
}

south_insyaa_ocean_region = {
	areas = {
		south_insyaa_sea_area
	}
}

punctured_coast_region = {
	areas = {
		punctured_coast_area
	}
}

west_aelantir_coast_region = {
	areas = {
		west_aelantir_coast_area
	}
}

torn_sea_region = {
	areas = {
		torn_sea_area
	}
}

outer_torn_sea_region = {
	areas = {
		outer_torn_sea_area
	}
}


northern_pass_region = {
	areas = {
		northern_pass_area
		inner_sea_area
		outer_sea_area
	}
}

ringlet_sea_region = {
	areas = {
		ringlet_sea_area
		gulf_of_rahen_area
		the_bridge_area
	}
}

gulf_of_rahen_region = {
	areas = {
		gulf_of_rahen_area
	}
}

northeast_haless_coast_region = {
	areas = {
		northeast_haless_coast_area
	}
}

kedwali_gulf_region = {
	areas = {
		kedwali_gulf_area 
	}
}

dameshead_sea_region = {
	areas = {
		damesneck_sea_area
		dameshead_sea_area
	}
}

giants_grave_sea_region = {
	areas = {
		giants_grave_sea_area
		bay_of_chills_area
	}
}

divenhal_sea_region = {
	areas = {
		bay_of_wines_sea_area
		west_diven_area
		coast_of_akan_area
		east_diven_area
		sea_of_follies_area
		sea_of_stone_area
		gulf_of_glass_area
		brasanni_sea_area
	}
}

westcoast_region = {
	areas = {
		southcoast_area
		westcoast_area
		bay_of_dragons_area
	}
}

northern_thaw_region = {
	areas = {
		reaversea_area
		far_thaw_area
		lonely_sea_area
	}
}

broken_sea_region = {
	areas = {
		broken_sea_approach_area
		broken_sea_area
		western_broken_sea_area
	}
}

western_broken_sea_region = {
	areas = {
		western_broken_sea_area
	}
}

ruined_sea_region = {
	areas = {
		trollsbay_coast_area
		ruined_sea_area
		east_ruin_sea
		western_respite_sea
		reapers_coast_area
	}
}

calamity_pass_region = {
	areas = {
		calamity_pass_area
	}
}







# Deprecated Stuff to prevent [triggerimplementation.cpp:15371]: Unknown region used in a trigger horn_of_africa_region crashes

########################################
# EUROPE                               #
########################################

france_region = {
	areas = {

	}
}

scandinavia_region = {
	areas = {

	}
}


low_countries_region = {
	areas = {

	}

}

italy_region = {
	areas = {

	}
}


north_german_region = {
	areas = { 

	}
}




south_german_region = {
	areas = { 

	}
}

russia_region = {
	areas = {

	}
}


ural_region = {
	areas = {

	}
}

iberia_region = {
	areas = {

	}
}



british_isles_region = {
	areas = {

	}
}

baltic_region = {
	areas = {

	}
}

poland_region = {
	areas = {
		
	}
}

ruthenia_region = {
	areas = {

	}
}

crimea_region = {
	areas = {

	}
}

balkan_region = {
	areas = {

	}
}

carpathia_region = {
	areas = {

	}
}

egypt_region = {
	areas = {

	}
}

maghreb_region = {
	areas = {

	}
}

mashriq_region = {
	areas = {

	}
}

anatolia_region = {
	areas = {

	}
}

persia_region = {
	areas = {

	}
}

khorasan_region = {
	areas = {

	}
}


caucasia_region = {
	areas = {

	}
}

arabia_region = {
	areas = {

	}
}

niger_region = {
	areas = {

	}
}

guinea_region = {
	areas = {

	}
}


sahel_region = {
	areas = {

	}
}

horn_of_africa_region = {
	areas = {

	}
}

east_africa_region = {
	areas = {

	}
}

central_africa_region = {
	areas = {

	}
}

kongo_region = {
	areas = {

	}
}

central_asia_region = {
	areas = {

	}
}

south_africa_region = {
	areas = {

	}
}

west_siberia_region = {
	areas = {

	}
}

east_siberia_region = {
	areas = {

	}
}

mongolia_region = {
	areas = {

	}
}

manchuria_region = {
	areas = {

	}
}

korea_region = {
	areas = {

	}
}

tibet_region = {
	areas = {

	}
}

hindusthan_region = {
	areas = {

	}
}

bengal_region = {
	areas = {

	}
}

west_india_region = {
	areas = {
		
	}
}

deccan_region = {
	areas = {

	}
}

coromandel_region = {
	areas = {

	}
}


burma_region = {
	areas = {

	}
}

japan_region = {
	areas = {

	}
}

australia_region = {
	areas = {

	}
}


south_china_region = {
	areas = {

	}
}

xinan_region = {
	areas = {

	}
}

north_china_region = {
	
	areas = {

	}
}

brazil_region = {
	
	areas = {

	}
}

la_plata_region = {
	areas = {

	}
}


colombia_region = {
	areas = {
	
	}
}

peru_region = {
	areas = {

	}
}

upper_peru_region = {
	areas = {

	}
}

malaya_region = {	#Western Indonesia Malaya.
	areas = {

	}
}


moluccas_region = {
	areas = {

	}
}



indonesia_region = { #Eastern Indonesian/Malayan Islands.
	areas = {

	}
}


oceanea_region = {
	areas = {

	}
}

indo_china_region = {
	areas = {

	}
}

canada_region = {
	areas = {

	}
}

great_lakes_region = {
	areas = {

	}
}

northeast_america_region = {
	areas = {

	}
}

southeast_america_region =  {
	areas = {

	}
}

mississippi_region = {
	areas = {

	}
}

great_plains_region = {
	areas = {

	}
}

california_region = {
	areas = {

	}
}

cascadia_region = {
	areas = {

	}
}

hudson_bay_region = {
	areas = {

	}
}

mexico_region = {
	areas = {

	}
}

central_america_region = {
	areas = {

	}
}

carribeans_region = {
	areas = {

	}
}

#Sea Regions
baltic_sea_region = {
	areas = {

	}
}

north_atlantic_region = {
	areas = {

	}
}

american_east_coast_region = {
	areas = {

	}
}

mediterrenean_region = {
	areas = {

	}
}

caribbean_sea_region = {
	areas = {

	}
}


west_african_sea_region = {
	areas = {

	}
}

west_indian_ocean_region = {
	areas = {

	}
}

arabian_sea_region = {
	areas = {

	}
}

east_indian_ocean_region = {
	areas = {

	}
}

south_indian_ocean_region = {
	areas = {

	}
}

south_china_sea_region = {
	areas = {

	}
}

east_china_sea_region = {
	areas = {

	}
}

north_west_pacific_region = {
	areas = {

	}
}

south_west_pacific_region = {
	areas = {

	}
}

south_east_pacific_region = {
	areas = {

	}
}

north_east_pacific_region = {
	areas = {

	}
}

pacific_south_america_region = {
	areas = {

	}
}

atlantic_south_america_region = {
	areas = {

	}
}

south_atlantic_region = {
	areas = {

	}
}
